using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW27012021
{
    class FizzBuzzEventArgs : EventArgs
    {
        public int Number { get; set; }
    }
    class Program
    {
        static public event EventHandler<FizzBuzzEventArgs> FizzOccured;
        static public event EventHandler<FizzBuzzEventArgs> BuzzOccured;
        static void FireEventFizz(int number)
        {
            if (FizzOccured != null)
            {
                FizzOccured.Invoke(typeof(Program), new FizzBuzzEventArgs { Number = number });
            }
        }
        static void FireEventBuzz(int number)
        {
            if (BuzzOccured != null)
            {
                BuzzOccured.Invoke(typeof(Program), new FizzBuzzEventArgs { Number = number });
            }
        }

        private static void HandleFizz(object sender, FizzBuzzEventArgs e)
        {
            Console.Write($"Fizz[{e.Number}] ");
        }
        private static void HandleBuzz(object sender, FizzBuzzEventArgs e)
        {
            Console.Write($"Buzz[{e.Number}] ");
        }
        static void Main(string[] args)
        {
            FizzOccured += HandleFizz;
            BuzzOccured += HandleBuzz;
            for (int i = 1; i <= 30; i++)
            {
                Console.Write(i + ": ");
                if (i % 3 == 0)
                {
                    // fire fizz event
                    FireEventFizz(i);
                }
                if (i % 5 == 0)
                {
                    // fire buzz event
                    FireEventBuzz(i);
                }
                Console.WriteLine();
            }


        }
    }
}
